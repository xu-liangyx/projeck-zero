package NGuessNumber;

import java.util.LinkedList;
import java.util.Scanner;

interface IGame {
	void initRandom();
	
	int guess();
	
	void time();
	
	void showHis();
}

class GuessGame implements IGame {

	LinkedList<Integer> randomList = new LinkedList<Integer>();
	LinkedList<String> hisList = new LinkedList<String>();
	
	public void initRandom() {
		for (int n = 1; n <= 4; n++) {
			randomList.add((int)(Math.random() * 10));
		}
	}
	public int guess() {
		System.out.println("\n请输入4个数字");
		Scanner scan = new Scanner(System.in);
		int cnt = 0;
		String once = "";
		for (int i = 0; i < 4; i++) {
			int userInNumber = scan.nextInt();
			once += userInNumber + " ";
			int fromListNumber = randomList.get(i);
			if (userInNumber == fromListNumber) {
				cnt++;
			}
		}
		hisList.add(once + ", 正确：" + cnt);
		return cnt;
	}

	public void time() {
		for (int k = 1; k <= 10; k++) {
			int count = guess();
			if (count == 4) {
				System.out.println("对了!!");
			} else if (count == 0) {
				System.out.println("没有正确的...");
			} else {
				System.out.println("有" + count + "个正确的");
			}
			System.out.println("输入“xl”查看历史，输入其他继续");
			Scanner scan = new Scanner(System.in);
			String userChoose = scan.next();
			if (userChoose.equals("xl")) {
				showHis();
			}
		}
	}

	public void showHis() {
		System.out.println("----start----");
		for (String temp : hisList) {
			System.out.println(temp);
		}
		System.out.println("----end----");
	}
	
	public void start() {
		initRandom();
		time();
	
	}
	
}

public class NGuessNumber {
	public static void main(String[] args) {
		GuessGame g = new GuessGame();
		g.start();
	}
}