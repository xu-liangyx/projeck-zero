package com.java;

import java.util.Random;
import java.util.Scanner;

public class Game {

    public static void main(String[] args) {
        Random r = new Random();
        int randomNum = r.nextInt(100) + 1;
        Scanner sc = new Scanner(System.in);

        for (int num = 1; num <= 8; num++) {
            System.out.println("请输入你猜的数字：");
            int guessNum = sc.nextInt();

            if(num == 7){
                if (randomNum == guessNum) {
                    System.out.println("恭喜你第7次终于猜对了");
                }else {
                    System.out.println("答案是：" + randomNum);
                    System.out.println("猜测次数全部用尽，请重新开始游戏");
                }
                break;
            }
            if (randomNum < guessNum){
                System.out.println("太大，请重试");
            }else if(randomNum > guessNum){
                System.out.println("太小，请重试");
            }else {
                System.out.println("恭喜你，猜对了");
                System.out.println("一共猜了：" +  num  +  "次");
                break;
            }
        }
    }

    }

